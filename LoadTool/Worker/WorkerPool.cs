﻿using LoadTool.Config;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadTool.Worker
{
    public class WorkerPool
    {
        private readonly BlockingCollection<Action> _queue = new BlockingCollection<Action>();
        private readonly Logger _log;
        private readonly Task[] _tasks;
        private int _activeWorkers;
        private int _totalWorkers;
        private readonly Task _updateTask;

        public WorkerPool(ParallelAccessOptions options, CancellationToken ct, Logger log)
        {
            _log = log;
            _totalWorkers = options.WorkerCount;
            _tasks = Enumerable.Range(0, options.WorkerCount).Select(_ => Task.Run(() => executeItemsWhenReady(ct), ct)).ToArray();
            _updateTask = Task.Run(() =>
            {
                while (true) {
                    _log.UpdateTitle($"Active Workers {_activeWorkers}/{_totalWorkers}");
                    Task.Delay(200);
                }
            });
        }

        private void executeItemsWhenReady(CancellationToken ct)
        {
            foreach (var item in _queue.GetConsumingEnumerable(ct))
            {
                try
                {
                    Interlocked.Increment(ref _activeWorkers);
                    item();
                }
                finally
                {
                    Interlocked.Decrement(ref _activeWorkers);
                }
            }            
        }

        public void AddWork(Action work)
        {
            _queue.Add(work);
        }

        public async Task WaitUntilFinishedAsync()
        {
            _queue.CompleteAdding();
            await Task.WhenAll(_tasks);
        }
    }
}
