﻿using LoadTool.Config;
using System;
using System.Collections.Generic;
using System.IO;

namespace LoadTool.Operation
{
    internal class DeleteFileOperation : FileOperation
    {
        private readonly Logger _log;

        public DeleteFileOperation(DeleteFiles config, Logger log) : base(config)
        {
            _log = log;
        }

        public override IEnumerable<Action> EnumerateActions()
        {
            foreach (var file in Directory.EnumerateFiles(TestPath, "*", SearchOption.AllDirectories))
            {
                yield return () =>
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                    _log.Write($"Deleted File {file}");
                };
            }
        }
    }
}