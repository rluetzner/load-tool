﻿using LoadTool.Config;
using System;
using System.Collections.Generic;
using System.IO;

namespace LoadTool.Operation
{
    public class CreateOperation : FileOperation
    {
        private static readonly Random _random = new Random();

        private readonly int _fileCount;
        private readonly SizeRange _range;
        private readonly Logger _log;

        public CreateOperation(CreateFiles config, Logger log) : base(config)
        {
            _fileCount = config.FileCount;
            _range = config.FileSize;
            _log = log;
        }

        public override IEnumerable<Action> EnumerateActions()
        {
            var currentPath = TestPath;
            for (int i = 0; i < _fileCount; i++)
            {
                var path = Path.Combine(TestPath, i.ToString());
                yield return () =>
                {
                    if (!Directory.Exists(currentPath))
                    {
                        Directory.CreateDirectory(currentPath);
                        _log.Write($"Created {currentPath}");
                    }
                    var bytes = new byte[_range.GetRandomFileSize()];
                    _random.NextBytes(bytes);                    
                    using (var fs = new FileStream(path, FileMode.CreateNew, FileAccess.Write, FileShare.Read))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    _log.Write($"Created {path} with {bytes.Length}");
                };
            }
        }
    }
}
