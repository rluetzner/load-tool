﻿using LoadTool.Config;
using System;
using System.Collections.Generic;

namespace LoadTool.Operation
{
    public abstract class FileOperation
    {
        protected readonly string TestPath;

        protected FileOperation(TestOperation config)
        {
            TestPath = config.TestPath;
        }

        protected FileOperation() { }

        public abstract IEnumerable<Action> EnumerateActions();
    }
}
