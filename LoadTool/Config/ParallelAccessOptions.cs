﻿namespace LoadTool.Config
{
    public class ParallelAccessOptions
    {
        public int WorkerCount { get; set; } = 4;
    }
}
