﻿using System;

namespace LoadTool.Config
{
    public class SizeRange
    {
        private static readonly Random _random = new Random();

        public long Minimum { get; set; } = 64 * 1024;
        public long Maximum { get; set; } = 128 * 1024;

        internal long GetRandomFileSize()
        {
            int diff = (int) (Maximum - Minimum);
            return _random.Next(diff) + Minimum;
        }
    }
}
