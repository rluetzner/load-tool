﻿namespace LoadTool.Config
{
    public class TestOperation
    {
        public string TestPath { get; set; }
        public bool Timed { get; set; } = false;

        protected TestOperation() { }
    }
}
