﻿namespace LoadTool.Config
{
    public class TestConfiguration
    {
        public TestOperation FileOperation { get; set; } = new CreateFiles();
        public ParallelAccessOptions ParallelOptions { get; set; } = new ParallelAccessOptions();
    }
}
