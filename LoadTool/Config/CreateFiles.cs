﻿namespace LoadTool.Config
{
    public class CreateFiles : TestOperation
    {        
        public int FileCount { get; set; }
        public SizeRange FileSize { get; set; } = new SizeRange();
    }
}
