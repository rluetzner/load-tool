﻿using LoadTool.Config;
using LoadTool.Operation;
using LoadTool.Worker;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace LoadTool.Builder
{
    internal static class TestBuilder
    {
        public static Test GetTest(TestConfiguration config)
        {
            var cts = new CancellationTokenSource();
            var fileOps = FileOperationBuilder.GetFileOperations(config.FileOperation);
            var workerPool = new WorkerPool(config.ParallelOptions, cts.Token, new Logger());
            return new Test(workerPool, fileOps);
        }

        private static class FileOperationBuilder
        {
            public static FileOperation GetFileOperations(TestOperation testOps)
            {
                FileOperation ops;
                switch (testOps)
                {
                    case CreateFiles cf:
                        ops = new CreateOperation(cf, new Logger());
                        break;
                    case DeleteFiles df:
                        ops = new DeleteFileOperation(df, new Logger());
                        break;
                    default:
                        throw new NotImplementedException();
                }
                if (testOps.Timed)
                {
                    ops = new TimedOperationWrapper(ops, new Logger());
                }
                return ops;
            }

            private class TimedOperationWrapper : FileOperation
            {
                private readonly FileOperation _wrappedOps;
                private readonly Logger _log;

                public TimedOperationWrapper(FileOperation ops, Logger log)
                {
                    _wrappedOps = ops;
                    _log = log;
                }

                public override IEnumerable<Action> EnumerateActions()
                {
                    foreach (var action in _wrappedOps.EnumerateActions())
                    {
                        yield return () =>
                        {
                            var sw = Stopwatch.StartNew();
                            action();
                            sw.Stop();
                            _log.Write($"Operation duration: {sw.Elapsed}");
                        };
                    }
                }
            }
        }
    }
}
