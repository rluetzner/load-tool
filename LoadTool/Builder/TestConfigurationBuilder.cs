﻿using LoadTool.Config;
using System;

namespace LoadTool.Builder
{
    internal class TestConfigurationBuilder
    {
        private TestOperation _testOperation;
        private bool _timed;
        private ParallelAccessOptions _parallelOptions = new ParallelAccessOptions();
        private string _testPath;
                
        public TestConfigurationBuilder CreateFiles(int count, long minSizeBytes, long maxSizeBytes)
        {
            _testOperation = new CreateFiles
            {
                FileCount = count,
                FileSize = new SizeRange
                {
                    Minimum = minSizeBytes,
                    Maximum = maxSizeBytes
                }
            };
            return this;
        }

        public TestConfigurationBuilder SetWorkerCount(int count)
        {
            _parallelOptions.WorkerCount = count;
            return this;
        }

        public TestConfigurationBuilder AddTimer()
        {
            _timed = true;
            return this;
        }

        public TestConfigurationBuilder DeleteFiles()
        {
            _testOperation = new DeleteFiles();
            return this;
        }

        public TestConfigurationBuilder InPath(string path)
        {
            _testPath = path;
            return this;
        }

        public TestConfiguration Build()
        {
            if (_testOperation == null) throw new ArgumentException("Cannot run tests without a file operation.");
            if (string.IsNullOrEmpty(_testPath)) throw new ArgumentException("Every test needs a test path.");
            _testOperation.TestPath = _testPath;
            _testOperation.Timed = _timed;
            return new TestConfiguration
            {
                FileOperation = _testOperation,
                ParallelOptions = _parallelOptions
            };
        }
    }
}
