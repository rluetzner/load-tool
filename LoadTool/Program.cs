﻿using LoadTool.Builder;

namespace LoadTool
{
    class Program
    {
        public static void Main(string[] args)
        {
            var testPath = @"C:\Temp\Test1";
            var config = new TestConfigurationBuilder()
                .CreateFiles(3000, 64 * 1024, 128 * 1024)
                .InPath(testPath)
                .SetWorkerCount(8)
                .AddTimer()
                .Build();            
                                   
            var test = TestBuilder.GetTest(config);
            test.Execute();

            var deleteConfig = new TestConfigurationBuilder()
                .DeleteFiles()
                .InPath(testPath)
                .SetWorkerCount(4)
                .Build();            
            test = TestBuilder.GetTest(deleteConfig);
            test.Execute();
        }
    }
}
