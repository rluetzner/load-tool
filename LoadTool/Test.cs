﻿using LoadTool.Operation;
using LoadTool.Worker;

namespace LoadTool
{

    public class Test
    {
        private readonly WorkerPool _pool;
        private readonly FileOperation _operation;

        public Test(WorkerPool pool, FileOperation operation)
        {
            _pool = pool;
            _operation = operation;
        }

        public void Execute()
        {
            var actions = _operation
                .EnumerateActions();
            foreach (var item in actions)
            {
                _pool.AddWork(item);
            }
            _pool.WaitUntilFinishedAsync().Wait();
        }
    }
}
